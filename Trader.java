/**
 * The type Trader.
 *
 *@author Stefan Sunken 4490128 Gruppe 11b
 *@author Marcel Warmbold 4492171 Gruppe 11b
 */
public class Trader extends Character {

    /**
     * Erstellt einen Haendler
     */
    public Trader() {
        super(100, 1, 1, 10000, 0.8);
        fillInventory();
    }
}
